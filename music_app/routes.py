import csv
import os.path
import re
from ast import literal_eval
from datetime import datetime
from secrets import token_urlsafe
from tempfile import NamedTemporaryFile

import chardet
from flask import (
    Blueprint,
    after_this_request,
    current_app,
    flash,
    json,
    make_response,
    redirect,
    render_template,
    request,
    send_from_directory,
    session,
    url_for,
)
from flask_login import current_user, login_required
from more_itertools import chunked

from music_app import cache, db
from music_app.forms import UploadForm

from .spotify import ParsePlaylists, SpotifyHelper

bp = Blueprint("routes", __name__, static_folder="./static")

sh = SpotifyHelper()
p = ParsePlaylists()
redirect_url = sh.REDIR_URL
session_expired = """
    Your session has expired. Click "Start Your Session" to start a new one."""
default_state = os.getenv("SPOTIFY_DEFAULT_STATE")


def get_endpoint(key):
    with open(os.path.join(bp.static_folder, "JSON/endpoints.json")) as f:
        d = json.load(f)
    return d[key]["url"]


def list_extractor(list, key):
    """Extract a list of values from a single key in list of dictionaries."""
    values = [item[key] for item in list]
    return values


@bp.route("/")
@bp.route("/index", methods=["GET", "POST"])
def index():
    return render_template("index.html")


@bp.route("/action", methods=["GET", "POST"])
@login_required
def action():
    if current_user.is_authenticated:
        if current_user.state == default_state:
            current_user.state = token_urlsafe(12)
            db.session.commit()
        ts = current_user.token_json
        tct = current_user.token_creation_time
        status = sh.token_status(token_string=ts, token_creation_time=tct)

        if session.get("shared") or status == "expired" or status == "no_token":
            return redirect(url_for("routes.new_request"))
        else:
            return redirect(url_for("routes.profile"))
    else:
        return redirect(url_for("routes.index"))


@bp.route("/new_request", methods=["GET", "POST"])
@login_required
def new_request():
    params = sh.AUTH_DICT
    params["state"] = current_user.state
    url = sh.auth_request(**params)
    return redirect(url)


@bp.route("/callback", methods=["GET", "POST"])
@login_required
def callback():
    # Check that 'state' strings match.
    if current_user.state != request.args.get("state"):
        raise Exception("Cross-site request forgery")

    # Store the code so ProfileData.token_header() can access it.
    if request.args.get("code"):
        session["auth_code"] = request.args.get("code")
        return redirect(url_for("routes.profile"))
    else:
        flash("Something went wrong.")
        return redirect(url_for("routes.index"))


def token_header():
    ts = current_user.token_json
    tct = current_user.token_creation_time
    code = session.get("auth_code")
    if code:
        session["auth_code"] = ""  # Clear this after code variable is set.
    try:
        tj = sh.token_json(token_string=ts, token_creation_time=tct, code=code)
        if str(tj) != ts:
            current_user.token_json = str(tj)
            current_user.token_creation_time = datetime.utcnow()
            db.session.commit()
        token = tj["access_token"]
        token_type = tj["token_type"]
        header = {"Authorization": token_type + " " + token}
        return header
    except KeyError:
        current_app.logger.exception("Token expired")
        return None


def make_key_prefix():
    return str(current_user) + "_playlist_api"


class ProfileData:
    """Collect data to populate the 'profile' view."""

    def __init__(self):
        self.header = token_header()

    def mock_token_header(self):  # Pytest (../tests/conftest.py) needs this.
        pass

    def profile_(self):
        return sh.get_data(self.header, get_endpoint("get-current-user-profile"))[2]

    @cache.cached(key_prefix=make_key_prefix)
    def playlists_(self, etags=None):
        header = self.header
        playlists = []
        i = 0

        # Log when cached playlists are not being used, i.e. a fresh API call.
        current_app.logger.info(
            f"Running playlists_ for {current_user}; etags = {etags}"
        )
        if etags:
            etag_list = literal_eval(etags)
            header["If-None-Match"] = (", ").join(etag_list)

        base_url = get_endpoint("get-list-of-user-playlists") + "{args}"
        id = session["user"]["id"]
        url_args = "?limit=50"
        url = base_url.format(user_id=id, args=url_args)
        data = sh.get_data(header, url)
        resp_etags = [data[1].get("etag")]
        pl_json = data[2]
        playlists += pl_json["items"]
        while pl_json["next"]:
            i += 1
            url_args = pl_json["next"][pl_json["next"].index("?") :]
            url = base_url.format(user_id=id, args=url_args)
            data = sh.get_data(header, url)
            resp_etags.append(data[1].get("etag"))
            pl_json = data[2]
            playlists += pl_json["items"]

        resp_dict = dict(items=playlists, total=len(playlists), resp_etags=resp_etags)

        return resp_dict

    def history_(self):
        return sh.get_data(
            self.header, get_endpoint("get-current-user-recently-played-tracks")
        )[2]

    def top_artists_(self):
        # Build endpoint url for top artists.
        top_a_url = get_endpoint("get-user-top-artists+tracks").format(type="artists")
        top_a_url = top_a_url + "?limit=50"

        return sh.get_data(self.header, top_a_url)[2]

    def top_tracks_(self):
        # Build endpoint url for top tracks.
        top_t_url = get_endpoint("get-user-top-artists+tracks").format(type="tracks")
        top_t_url = top_t_url + "?limit=50"

        return sh.get_data(self.header, top_t_url)[2]


@bp.route("/api/<endpoint>", methods=["GET"])
@login_required
def api_call(endpoint):
    # Use this to view json response from api calls.
    api = ProfileData()
    func = api.__getattribute__(endpoint + "_")

    resp = make_response(func())

    return resp


@bp.route("/profile", methods=["GET", "POST"])
@login_required
def profile():
    """Snapshot page of current user's profile.

    Displays playlists, recently played tracks,
    top artists, and tops tracks.
    """
    pd = ProfileData()
    if not pd.header and not current_app.testing:
        flash(session_expired)
        return redirect(url_for("routes.index"))

    missing = session.get("missing")
    retry_missing = session.get("retry_missing")
    with open(os.path.join(bp.static_folder, "JSON/mock.json")) as f:
        mock = json.load(f)
    err_msg = "We could not retrieve "
    new_etags = []
    cookie_name = session["_user_id"] + "_playlist_etags"

    if not session.get("user"):
        try:
            profile = pd.profile_()
        except Exception as e:
            current_app.logger.exception(f"{e}: Could not retrieve user profile.")
            profile = mock["profile"]
            flash(err_msg + "user profile.")
        session["user"] = profile

    try:
        etag_cookie = request.cookies.get(cookie_name)
        playlists = pd.playlists_(etags=etag_cookie)
        new_etags = playlists["resp_etags"]
        playlist_names = [item["name"] for item in playlists["items"]]
        if session.get("playlists") != playlist_names:
            session["playlists"] = playlist_names
    except Exception as e:
        current_app.logger.exception(f"{e}: Could not retrieve playlists.")
        playlists = mock["playlists"]
        flash(err_msg + "playlists.")

    try:
        history = pd.history_()
    except Exception as e:
        current_app.logger.exception(f"{e}: Could not retrieve recently played songs.")
        history = mock["history"]
        flash(err_msg + "recently played songs.")

    try:
        top_artists = pd.top_artists_()
    except Exception as e:
        current_app.logger.exception(f"{e}: Could not retrieve top artists.")
        top_artists = mock["top_artists"]
        flash(err_msg + "your top artists.")

    try:
        top_tracks = pd.top_tracks_()
    except Exception as e:
        current_app.logger.exception(f"{e}: Could not retrieve top tracks.")
        top_tracks = mock["top_tracks"]
        flash(err_msg + "your top tracks.")

    resp = make_response(
        render_template(
            "profile.html",
            playlists=playlists["items"][:20],
            history=history["items"],
            top_artists=top_artists["items"],
            top_tracks=top_tracks["items"],
            list_extractor=list_extractor,
            missing=missing,
            retry_missing=retry_missing,
            title="Playlist-ify | Your Spotify Music",
        )
    )

    if str(new_etags) != etag_cookie:
        resp.set_cookie(cookie_name, value=str(new_etags), samesite="Strict")

    return resp


@bp.route("/playlists", methods=["GET"])
@login_required
def playlists():
    pl = ProfileData()
    if not pl.header and not current_app.testing:
        flash(session_expired)
        return redirect(url_for("routes.index"))

    cookie_name = session["_user_id"] + "_playlist_etags"
    try:
        etag_cookie = request.cookies.get(cookie_name)
        playlists = pl.playlists_(etags=etag_cookie)
        new_etags = playlists["resp_etags"]
        playlist_names = [item["name"] for item in playlists["items"]]
        if session.get("playlists") != playlist_names:
            session["playlists"] = playlist_names
    except Exception as e:
        current_app.logger.exception(f"{e}: Could not retrieve playlists.")

    resp = make_response(
        render_template(
            "playlists.html", playlists=playlists["items"], title="Playlists"
        )
    )

    if str(new_etags) != etag_cookie:
        resp.set_cookie(cookie_name, value=str(new_etags), samesite="Strict")

    return resp


def add_tracks(found, header, playlist_id):
    # Format endpoint URL from static/JSON/endpoints.json.
    url_add = get_endpoint("add-items-to-playlist").format(playlist_id=playlist_id)

    # Maximum of 100 tracks can be added at once.
    if 0 < len(found["uris"]) <= 100:
        add_tracks = sh.post_data(header, url_add, **found)
        if add_tracks[0] in [200, 201]:
            return True

    # For more than 100 tracks, process in chunks.
    elif len(found["uris"]) > 100:
        count = 0
        chunks = chunked(found["uris"], 100)
        for chunk in chunks:
            found_chunk = {}
            found_chunk["uris"] = chunk
            add_tracks = sh.post_data(header, url_add, **found_chunk)
            if add_tracks[0] in [200, 201]:
                count += 1
        if count > 0:
            return True
    else:
        return False


@bp.route("/upload", methods=["GET", "POST"])
@login_required
def upload():
    header = token_header()
    if not header and not current_app.testing:
        flash(session_expired)
        return redirect(url_for("routes.index"))

    # Clear 'missing' and 'retry_missing' from session.
    session["missing"] = []
    session["retry_missing"] = []

    # Delete cache
    key = make_key_prefix()
    cache.delete(key)
    if request.args.get("refresh"):
        cookie_name = session["_user_id"] + "_playlist_etags"
        referrer = request.referrer.split("/")[-1]
        if referrer != "upload":
            resp = make_response(redirect(url_for("routes." + referrer)))
        else:
            resp = make_response(redirect(url_for("routes.profile")))
        resp.delete_cookie(cookie_name)
        return resp

    # Upload the playlist file.
    form = UploadForm()
    err_tracks = []
    message = ""
    if form.validate_on_submit():
        # Use the file name as the playlist name.
        file = form.upload.data
        filename = file.filename
        playlist_name = filename.split(".")[0]

        # Parse an xml playlist file.
        if filename.split(".")[-1] == "xml":
            songs = p.parse_songs_xml(file)

        # Parse a csv or txt (actually tab-delimited csv) file.
        else:
            # Start with bytes, then encode.
            byte_str = file.read()
            try:
                u_string = byte_str.decode()
            except Exception as e:
                encoding = chardet.detect(byte_str)["encoding"].lower()
                u_string = byte_str.decode(encoding=encoding)
                current_app.logger.exception(e)
            songs = p.parse_songs_csv(u_string)

        # Create new playlist if it does not already exist.
        if playlist_name not in session.get("playlists", []):
            params = {"name": playlist_name, "public": False}

            # Get and format endpoint URL from static/JSON/endpoints.json.
            url_create = get_endpoint("create-playlist").format(
                user_id=session["user"]["id"]
            )

            # Create the playlist, check if `songs` available in Spotify.
            new_playlist = sh.post_data(header, url_create, **params)
            if new_playlist[0] in [200, 201]:
                playlist_id = new_playlist[1]["id"]
                tracks = sh.collect_tracks(header, session["user"]["country"], *songs)

                # Log and flash errors due to tracks with bad data.
                # See err_string below.

                for error in tracks["errors"]:
                    current_app.logger.exception(error)
                    error_track = re.findall(r"{'name': '(.+)', 'artist'", error)
                    err_tracks += error_track

                found = tracks["found"]
                missing_tracks = tracks["missing"]
                d = {
                    "playlist_id": playlist_id,
                    "playlist_name": playlist_name,
                    "tracks": [],
                }
                if add_tracks(found, header, playlist_id):
                    message = "Success! Your playlist was added. "
                else:
                    message = "We created a playlist" + " but couldn't add any tracks."

                # List tracks that were not found in Spotify.
                if missing_tracks:
                    missing_list = [list(song.values()) for song in missing_tracks]
                    missing = [(" | ").join(song) for song in missing_list]
                    d["tracks"] = missing

                    # Avoid repeat entries for session.missing playlists.
                    names = [i["playlist_name"] for i in session["missing"]]
                    if playlist_name not in names:
                        session["missing"].append(d)

                    if found["uris"]:
                        message = message + "We couldn't find some tracks."
            else:
                message = "Something went wrong -- playlist not created."
                error_msg = str(new_playlist[0]) + " - " + new_playlist[1]
                current_app.log_exception(error_msg)
        else:
            message = "You already have a playlist by that name."
        if err_tracks:
            err_str = "Tracks skipped due to data errors: "
            err_str += ", ".join(err_tracks)
            flash(err_str)
        flash(message)
        return redirect(url_for("routes.profile"))
    else:
        return render_template(
            "upload.html", form=form, title="Playlist-ify | Import a Playlist"
        )


@bp.route("/retry", methods=["GET", "POST"])
@login_required
def retry():
    """Try to match missing tracks by just using name and artist."""
    if not request.referrer:
        flash("Start here. 'Retry' only works after 'Import a Playlist'.")
        return redirect(url_for("routes.profile"))
    header = token_header()
    retry_missing = []
    for playlist in session["missing"]:
        songs = []
        playlist_id = playlist["playlist_id"]
        playlist_name = playlist["playlist_name"]

        # Make a dictionary from string of missing tracks.
        track_list = [track.split(" | ") for track in playlist["tracks"]]
        for track in track_list:
            tracks_dict = {}
            tracks_dict["name"] = track[0]
            tracks_dict["artist"] = track[1]
            songs.append(tracks_dict)  # Append track_dict to songs list.
        tracks = sh.collect_tracks(
            header,
            session["user"]["country"],
            *songs,
            include_album=False,
            filter_artist=True,  # Replaces '&' with a comma.
        )
        found = tracks["found"]

        success = "Success! We added tracks to " + playlist_name + "."
        if add_tracks(found, header, playlist_id):
            flash(success)

        if tracks["missing"]:
            missing_list = [list(song.values()) for song in tracks["missing"]]
            missing = [(" | ").join(song) for song in missing_list]
            d = {
                "playlist_id": playlist_id,
                "playlist_name": playlist_name,
                "tracks": missing,
            }
            retry_missing.append(d)
            flash("Some tracks are still missing from " + playlist_name + ".")
    session["retry_missing"] = retry_missing
    session["missing"] = []  # Clear missing list.
    return redirect(url_for("routes.profile"))


@bp.route("/download", methods=["GET"])
@login_required
def download(backup=False):
    @after_this_request
    def clear_tmp(response):
        temp_dir = current_app.config["TEMP_DIR"]
        user_tmp_files = [
            file
            for file in os.listdir(temp_dir)
            if file.startswith(session["user"]["id"])
        ]
        for file in user_tmp_files:
            os.remove(os.path.join(temp_dir, file))
        return response

    if not request.referrer:
        flash(
            "You can only access the download page from here " + "or from '/playlists'."
        )
        return redirect(url_for("routes.profile"))
    header = token_header()
    temp_dir = current_app.config["TEMP_DIR"]
    params = {}
    params["market"] = session["user"]["country"]
    backup = request.args.get("backup")

    if not backup:  # For playlist downloads
        playlist_id = request.args.get("id")
        file_name = request.args.get("name") + ".csv"
        url = get_endpoint("get-playlist-items").format(playlist_id=playlist_id)
        params["fields"] = (
            "items.track.name,"
            + "items.track.album.name,"
            + "items.track.artists,"
            + "next"
        )
        query_limit = 100

    if backup:  # For download of 'Liked Songs'
        playlist_id = "_liked_songs"
        file_name = "Liked Songs.csv"
        url = get_endpoint("get-user-saved-tracks")
        query_limit = 50

    tracks = []
    params["limit"] = query_limit
    section = sh.get_playlist_tracks(header, url, **params)
    tracks += section["items"]
    while section["next"]:
        url = section["next"]
        section = sh.get_playlist_tracks(header, url)
        tracks += section["items"]

    if not tracks:
        flash("Something went wrong. We couldn't get your playlist.")
        return redirect(url_for("routes.profile"))

    cookie_val = session["user"]["id"] + playlist_id

    with NamedTemporaryFile(
        mode="w+t",
        encoding="utf8",
        newline="",
        prefix=cookie_val + ".",
        dir=temp_dir,
        delete=False,
    ) as csvfile:
        fieldnames = ["Name", "Artist", "Album"]
        if backup:
            fieldnames += [
                "Date Added",
                "Spotify ID",
                "ISRC",
            ]
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames, delimiter="\t")
        writer.writeheader()
        for item in tracks:
            artists = ", ".join(list_extractor(item["track"]["artists"], "name"))
            rows = {
                "Name": item["track"]["name"],
                "Artist": artists,
                "Album": item["track"]["album"]["name"],
            }
            if backup:
                rows["Date Added"] = item["added_at"]
                rows["Spotify ID"] = item["track"]["id"]
                # rows["ISRC"] = item["track"]["external_ids"]["isrc"]
                rows["ISRC"] = item.get("track").get("external_ids").get("isrc")
            writer.writerow(rows)

    target_file = [
        file for file in os.listdir(temp_dir) if file.startswith(cookie_val)
    ][0]

    resp = make_response(
        send_from_directory(
            temp_dir, target_file, as_attachment=True, download_name=file_name
        )
    )

    resp.set_cookie("download", value=cookie_val, samesite="Strict")

    return resp


@bp.route("/logout_spotify", methods=["GET"])
def logout_spotify():
    logout_url = sh.spotify_logout()
    return redirect(logout_url)
