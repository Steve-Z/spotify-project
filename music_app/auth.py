from flask import (
    Blueprint,
    flash,
    make_response,
    redirect,
    render_template,
    session,
    url_for,
)
from flask_login import current_user, login_user, logout_user

from music_app import cache, db
from music_app.forms import LoginForm, RegistrationForm
from music_app.models import User
from music_app.routes import make_key_prefix

bp = Blueprint(
    "auth",
    __name__,
)


@bp.route("/login", methods=["GET", "POST"])
def login():
    if current_user.is_authenticated:
        return redirect(url_for("routes.index"))

    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()

        """ The following conditional is probably unnecessary since
        LoginForm validators for username & password were added. """
        if user is None or not user.check_password(form.password.data):
            flash("Invalid username or password")
            return redirect(url_for("auth.login"))

        if form.shared.data:
            session["shared"] = True
        else:
            session["shared"] = False
        login_user(user, remember=form.remember_me.data)
        return redirect(url_for("routes.index"))

    return render_template("login.html", title="Sign In", form=form)


@bp.route("/logout")
def logout():
    if session.get("shared"):
        redirect_url = "routes.logout_spotify"
    else:
        redirect_url = "routes.index"

    cookie_name = session["_user_id"] + "_playlist_etags"
    key = make_key_prefix()
    shared = session.get("shared")
    session.clear()
    logout_user()

    resp = make_response(redirect(url_for(redirect_url)))
    resp.delete_cookie(cookie_name)  # Need to do this. Not sure why.

    if shared:
        cache.delete(key)

    return resp


@bp.route("/register", methods=["GET", "POST"])
def register():
    if current_user.is_authenticated:
        return redirect(url_for("routes.index"))

    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash("Success! You are now a registered user.")
        return redirect(url_for("auth.login"))

    return render_template("register.html", title="Register", form=form)
